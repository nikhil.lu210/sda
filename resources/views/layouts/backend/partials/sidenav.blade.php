<div id="sidebar" class="sidebar py-3">
    <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
    <ul class="sidebar-menu list-unstyled">
        <li class="sidebar-list-item">
            <a href="{{ Route('admin') }}" class="sidebar-link text-muted {{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <i class="o-home-1 mr-3 text-gray"></i>
                <span>Dashboard</span>
            </a>
        </li>

        {{-- Contacts --}}
        <li class="sidebar-list-item">
            <a href="{{ Route('admin.contact') }}" class="sidebar-link text-muted {{ Request::is('admin/contact*') ? 'active' : '' }}">
                <i class="o-user-details-1 mr-3 text-gray"></i>
                <span>Contacts</span>
            </a>
        </li>

        {{-- Records --}}
        <li class="sidebar-list-item">
            <a href="#" data-toggle="collapse" data-target="#record" aria-expanded="{{ Request::is('admin/records/*') ? 'true' : 'false' }}" aria-controls="record" class="sidebar-link text-muted {{ Request::is('admin/records/*') ? 'active' : '' }}">
                <i class="o-paper-stack-1 mr-3 text-gray"></i>
                <span>Records</span>
            </a>
            <div id="record" class="collapse {{ Request::is('admin/records/*') ? 'show' : '' }}">
                <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.course') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/records/course*') ? 'active' : '' }}">All Courses</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.offer') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/records/offer*') ? 'active' : '' }}">All offers</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.teacher') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/records/teacher*') ? 'active' : '' }}">All Teachers</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.student') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/records/student*') ? 'active' : '' }}">All Students</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.connection') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/records/connection*') ? 'active' : '' }}">All Connections</a>
                    </li>
                </ul>
            </div>
        </li>

        {{-- Settings --}}
        <li class="sidebar-list-item">
            <a href="#" data-toggle="collapse" data-target="#setting" aria-expanded="{{ Request::is('admin/settings/*') ? 'true' : 'false' }}" aria-controls="setting" class="sidebar-link text-muted {{ Request::is('admin/settings/*') ? 'active' : '' }}">
                <i class="o-settings-window-1 mr-3 text-gray"></i>
                <span>Settings</span>
            </a>
            <div id="setting" class="collapse {{ Request::is('admin/settings/*') ? 'show' : '' }}">
                <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.course.create') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/settings/course*') ? 'active' : '' }}">Add Course</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.offer.create') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/settings/offer*') ? 'active' : '' }}">Add Offer</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.teacher.create') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/settings/teacher*') ? 'active' : '' }}">Add Teacher</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.student.create') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/settings/student*') ? 'active' : '' }}">Add Student</a>
                    </li>

                    <li class="sidebar-list-item">
                        <a href="{{ Route('admin.connection.create') }}" class="sidebar-link text-muted pl-lg-5 {{ Request::is('admin/settings/connection*') ? 'active' : '' }}">Add Connection</a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>
