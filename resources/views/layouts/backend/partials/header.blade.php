<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow"> 

    {{--  Page Title  --}}
    <title> SDA II @yield('page_title') </title>
    
    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('frontend/images/title.png') }}">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}"> {{--
    <link rel="stylesheet" href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css') }}"> --}} 
    
    {{-- Font-Awesome CSS --}}
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome/all.min.css') }}">

    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">

    <!-- orion icons-->
    <link rel="stylesheet" href="{{ asset('backend/css/orionicons.css') }}">

    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.default.css') }}" id="theme-stylesheet">

    
    @yield('css_links')

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">

    
    @yield('stylesheet')
</head>