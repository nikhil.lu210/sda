<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{--  Page Title  --}}
<title> SDA II @yield('page_title') </title>
<link rel="shortcut icon" href="{{ asset('frontend/images/title.png') }}" type="image/x-icon">

{{-- Fonts --}}
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900|Roboto:100,300,400,500,700,900" rel="stylesheet">

{{--  Bootstrap CSS  --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

{{--  Font-Awesome CSS  --}}
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome/all.min.css') }}">

@yield('css_links')

{{-- Custom CSS --}}
<link rel="stylesheet" href="{{ asset('frontend/css/margin_padding.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">


@yield('stylesheet')
