

{{-- ===========< Footer Part Start >=========== --}}
<footer class="footer-part p-t-100 p-b-50">
        <div class="container">
            @foreach ($connections as $connection)
                <div class="row">
                    <div class="col-md-6">
                        <div class="address">
                            <h5 class="p-b-20">Sylhet Development Academy</h5>

                            <ul>
                                <li><i class="p-r-10 fas fa-map-marker-alt"></i> {{ $connection->location }}, {{ $connection->city }}</li>
                                <li><i class="p-r-5 far fa-envelope"></i> {{ $connection->contact_mail }}</li>
                                <li><i class="p-r-10 fas fa-phone-volume"></i> {{ $connection->mobile }}</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="quick-links">
                            <h5 class="p-b-20">Quick Links</h5>

                            <ul>
                                <li><a href="{{ Route('homepage') }}">Home</a></li>
                                <li><a href="{{ Route('about') }}">About</a></li>
                                <li><a href="{{ Route('course') }}">Our Courses</a></li>
                                <li><a href="{{ Route('offer') }}">Offers</a></li>
                                <li><a href="{{ Route('contact') }}">Contact</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="social-links">
                            <h5 class="p-b-20">Social Links</h5>

                            <ul>
                                <li><a href="{{ $connection->facebook }}">Facebook</a></li>
                                <li><a href="{{ $connection->linkedin }}">Linkedin</a></li>
                                <li><a href="{{ $connection->twitter }}">Twitter</a></li>
                                <li><a href="{{ $connection->instagram }}">Instagram</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </footer>



    <section class="bottom-part p-t-30 p-b-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h6>Copyright © 2019 All rights reserved || by <a href="https://veechitechnologies.com" target="_blank" class="veechi">Veechi Technologies</a></h6>
                </div>
            </div>
        </div>
    </section>
    {{-- ============< Footer Part End >============ --}}
