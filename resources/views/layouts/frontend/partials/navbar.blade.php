<header>
    <div class="nav-wrapper">
        <div class="logo-container">
            {{-- <img class="logo" src="https://i.imgur.com/gea725J.png" alt="Logo"> --}}
            <a href="{{ Route('homepage') }}">
                <img class="logo" src="{{ asset('frontend/images/logo.png') }}" alt="Logo">
            </a>
        </div>
        <nav>
            <input class="hidden" type="checkbox" id="menuToggle">
            <label class="menu-btn" for="menuToggle">
                <div class="menu"></div>
                <div class="menu"></div>
                <div class="menu"></div>
            </label>
            <div class="nav-container">
                <ul class="nav-tabs">
                    <li class="nav-tab">
                        <a class="menu-link {{ Request::is('/') ? 'active' : '' }}" href="{{ Route('homepage') }}">Home</a>
                    </li>

                    <li class="nav-tab">
                        <a class="menu-link {{ Request::is('about*') ? 'active' : '' }}" href="{{ Route('about') }}">About</a>
                    </li>

                    <li class="nav-tab">
                        <a class="menu-link {{ Request::is('courses*') ? 'active' : '' }}" href="{{ Route('course') }}">Our Courses</a>
                    </li>

                    {{-- <li class="nav-tab">
                        <a class="menu-link {{ Request::is('offers*') ? 'active' : '' }}" href="{{ Route('offer') }}">Offers</a>
                    </li> --}}

                    <li class="nav-tab">
                        <a class="menu-link {{ Request::is('contact*') ? 'active' : '' }}" href="{{ Route('contact') }}">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
