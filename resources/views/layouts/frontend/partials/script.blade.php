<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

@yield('script_links')

<!-- Custom JS -->
<script src="{{ asset('frontend/js/script.js') }}"></script>
<script src="{{ asset('frontend/js/responsive.js') }}"></script>

@yield('scripts')
