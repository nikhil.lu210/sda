@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Contacts')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Contact Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                  <div class="card-header">
                    <h6 class="text-uppercase mb-0">All Contacts</h6>
                  </div>
                  <div class="card-body">
                    <table class="table card-text table-borderless table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th>Phone Number</th>
                          <th>Course</th>
                          <th>Message</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($contacts as $contact)
                            <tr>
                                <td>{{ $contact->name }}</td>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->phone }}</td>
                                <td>{{ $contact->course }}</td>
                                <td>{{ $contact->message }}</td>
                                <td class="action-td">
                                      <div class="action-btn">
                                          {{-- Delete Button --}}
                                          <a href="{{ route('admin.contact.destroy', ['id'=>$contact->id])}}" class="btn btn-outline-danger btn-outline-custom btn-delete btn-sm" onclick="return confirm('Are you sure to delete?')">
                                              <i class="far fa-trash-alt"></i>
                                          </a>

                                          {{-- View Button --}}
                                          <a href="#" class="btn btn-outline-success btn-outline-custom btn-view btn-sm">
                                              <i class="far fa-eye"></i>
                                          </a>
                                      </div>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Contact Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
