@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Connections II Create')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Connections Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                    <div class="card-header text-center">
                        <h6 class="text-uppercase mb-0">Add Information</h6>
                    </div>
                    <div class="line"></div>
                    <div class="card-body">

                        <form action="{{ route('admin.connection.store')}}" method="POST">
                        @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Location</label>
                                        <input type="text" placeholder="Location" name="location" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">City</label>
                                        <input type="text" placeholder="City" name="city" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Mobile Number</label>
                                        <input type="text" placeholder="Mobile Number" name="mobile" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Phone Number</label>
                                        <input type="text" placeholder="Phone Number" name="phone" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Contact Mail</label>
                                        <input type="text" placeholder="Contact Mail" name="contact_mail" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Information Mail</label>
                                        <input type="text" placeholder="Information Mail" name="info_mail" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Facebook</label>
                                        <input type="text" placeholder="Facebook" name="facebook" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Linkedin</label>
                                        <input type="text" placeholder="Linkedin" name="linkedin" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Twitter</label>
                                        <input type="text" placeholder="Twitter" name="twitter" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Instagram</label>
                                        <input type="text" placeholder="Instagram" name="instagram" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg float-right mt-2">Add Information</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Connections Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
