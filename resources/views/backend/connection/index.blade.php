@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Connections')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>
.card-header .btn-outline-custom{
    margin-top: -8px;
}
</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Connections Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 mb-4">

                @foreach ($connections as $connection)
                <div class="card shadow">
                    <div class="card-header">
                        <h6 class="text-uppercase mb-0 float-left">All Connections</h6>

                        <a href="{{ route('admin.connection.show', ['id'=>$connection->id])}}" class="btn btn-outline-success btn-outline-custom btn-view btn-sm float-right">
                            <i class="far fa-edit"></i>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table card-text table-borderless table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Location</th>
                                    <td>{{ $connection->location }}</td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td>{{ $connection->city }}</td>
                                </tr>
                                <tr>
                                    <th>Mobile Number</th>
                                    <td>{{ $connection->mobile }}</td>
                                </tr>
                                <tr>
                                    <th>Phone Number</th>
                                    <td>{{ $connection->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Contact Email</th>
                                    <td>{{ $connection->contact_mail }}</td>
                                </tr>
                                <tr>
                                    <th>Info Mail</th>
                                    <td>{{ $connection->info_mail }}</td>
                                </tr>
                                <tr>
                                    <th>Facebook</th>
                                    <td>{{ $connection->facebook }}</td>
                                </tr>
                                <tr>
                                    <th>Linkedin</th>
                                    <td>{{ $connection->linkedin }}</td>
                                </tr>
                                <tr>
                                    <th>Twitter</th>
                                    <td>{{ $connection->twitter }}</td>
                                </tr>
                                <tr>
                                    <th>Instagram</th>
                                    <td>{{ $connection->instagram }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
</div>
{{-- ==================< Connections Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
