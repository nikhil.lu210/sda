@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Connections II View')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Connections Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                    <div class="card-header text-center">
                        <h6 class="text-uppercase mb-0">Update Information</h6>
                    </div>
                    <div class="line"></div>
                    <div class="card-body">

                        <form action="{{ route('admin.connection.update', ['id'=>$connection->id])}}" method="POST">
                        @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Location</label>
                                        <input type="text" value="{{ $connection->location }}" name="location" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">City</label>
                                        <input type="text" value="{{ $connection->city }}" name="city" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Mobile Number</label>
                                        <input type="text" value="{{ $connection->mobile }}" name="mobile" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Phone Number</label>
                                        <input type="text" value="{{ $connection->phone }}" name="phone" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Contact Mail</label>
                                        <input type="text" value="{{ $connection->contact_mail }}" name="contact_mail" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Information Mail</label>
                                        <input type="text" value="{{ $connection->info_mail }}" name="info_mail" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Facebook</label>
                                        <input type="text" value="{{ $connection->facebook }}" name="facebook" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Linkedin</label>
                                        <input type="text" value="{{ $connection->linkedin }}" name="linkedin" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Twitter</label>
                                        <input type="text" value="{{ $connection->twitter }}" name="twitter" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Instagram</label>
                                        <input type="text" value="{{ $connection->instagram }}" name="instagram" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg float-right mt-2">Update Information</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Connections Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
