@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Teachers')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>
.teacher-part td{
    padding-top: 25px;
}
.teacher-part td.avatar-td{
    padding-top: 10px;
}
.teacher-part td.action-td{
    padding-top: 20px;
}
.teacher-part .avatar-td .img-fluid{
    max-width: 50px;
    border-radius: 5px;
}
</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Teachers Part Starts >================= --}}
<div class="container-fluid px-xl-5 teacher-part">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                    <div class="card-header">
                    <h6 class="text-uppercase mb-0">All Teachers</h6>
                    </div>
                    <div class="card-body">
                    <table class="table card-text table-borderless table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Teacher Name</th>
                                <th>Email</th>
                                <th>Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="avatar-td">
                                    <img src="{{ asset('backend/img/avatar-6.jpg') }}" alt="Jhon Doe" class="img-responsive img-fluid">
                                </td>
                                <td>Sujan Baiddya</td>
                                <td>baiddya.sujan@gmail.com</td>
                                <td>+880 1712 345678</td>
                                <td class="action-td">
                                    <div class="action-btn">
                                        {{-- Delete Button --}}
                                        <a href="#" class="btn btn-outline-danger btn-outline-custom btn-delete btn-sm" onclick="return confirm('Are you sure to delete?')">
                                            <i class="far fa-trash-alt"></i>
                                        </a>

                                        {{-- View Button --}}
                                        <a href="#" class="btn btn-outline-success btn-outline-custom btn-view btn-sm">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Teachers Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
