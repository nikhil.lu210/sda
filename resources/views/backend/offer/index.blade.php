@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Offers')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Offers Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                    <div class="card-header">
                    <h6 class="text-uppercase mb-0">All Offers</h6>
                    </div>
                    <div class="card-body">
                    <table class="table card-text table-borderless table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Offer Name</th>
                                <th>Offer Starts</th>
                                <th>Offer Ends</th>
                                <th>Discount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dhamaka Offer</td>
                                <td>02-05-2019</td>
                                <td>02-06-2019</td>
                                <td>27%</td>
                                <td class="action-td">
                                    <div class="action-btn">
                                        {{-- Delete Button --}}
                                        <a href="#" class="btn btn-outline-danger btn-outline-custom btn-delete btn-sm" onclick="return confirm('Are you sure to delete?')">
                                            <i class="far fa-trash-alt"></i>
                                        </a>

                                        {{-- View Button --}}
                                        <a href="#" class="btn btn-outline-success btn-outline-custom btn-view btn-sm">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Offers Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
