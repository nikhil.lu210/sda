@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Course II View')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Course Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 mb-4">
                <div class="card shadow">
                    <div class="card-header text-center">
                        <h6 class="text-uppercase mb-0">Course Name</h6>
                    </div>
                    <div class="line"></div>
                    <div class="card-body">

                        <form action="{{ route('admin.course.update', ['id'=>$course->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Avatar</label>
                                        <input type="file" value="{{ $course->avatar }}" name="avatar" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg file-type">
                                        @if ($errors->has('avatar'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('avatar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Category</label>
                                        <select name="category" class="form-control form-control-lg">
                                            <option value="{{$course->category}}" selected>
                                                @switch($course->category)
                                                    @case(1)
                                                        Designing
                                                        @break
                                                    @case(2)
                                                        Development
                                                        @break
                                                    @default
                                                        Others
                                                @endswitch
                                            </option>
                                            <option value="1">Designing</option>
                                            <option value="2">Development</option>
                                            <option value="3">Others</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Name</label>
                                        <input type="text" value="{{$course->name}}" name="name" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Short Note</label>
                                        <input type="text" value="{{$course->short_note}}" name="short_note" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Price</label>
                                        <input type="text" value="{{$course->price}}" name="price" class="form-control form-control-lg">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg float-right mt-2">Update Course</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Course Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
