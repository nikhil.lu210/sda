@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Courses')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Courses Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card shadow">
                    <div class="card-header">
                    <h6 class="text-uppercase mb-0">All Courses</h6>
                    </div>
                    <div class="card-body">
                    <table class="table card-text table-borderless table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Course Category</th>
                                <th>Course Name</th>
                                <th>Course Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($courses as $course)
                                <tr>
                                    {{-- @if ($course->category == 1)
                                        <td>Designing</td>
                                    @elseif($course->category == 2)
                                        <td>Development</td>
                                    @else
                                        <td>Others</td>
                                    @endif --}}

                                    @switch($course->category)
                                        @case(1)
                                            <td>Designing</td>
                                            @break

                                        @case(2)
                                            <td>Development</td>
                                            @break

                                        @default
                                            <td>Others</td>
                                    @endswitch

                                    <td>{{ $course->name }}</td>
                                    <td>{{ $course->price }} <sup>TK</sup></td>
                                    <td class="action-td">
                                        <div class="action-btn">
                                            {{-- Delete Button --}}
                                            <a href="{{ route('admin.course.destroy', ['id'=>$course->id])}}" class="btn btn-outline-danger btn-outline-custom btn-delete btn-sm" onclick="return confirm('Are you sure to delete?')">
                                                <i class="far fa-trash-alt"></i>
                                            </a>

                                            {{-- View Button --}}
                                            <a href="{{ Route('admin.course.show', ['id'=>$course->id]) }}" class="btn btn-outline-success btn-outline-custom btn-view btn-sm">
                                                <i class="far fa-eye"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Courses Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
