@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Course II Create')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Course Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 mb-4">
                <div class="card shadow">
                    <div class="card-header text-center">
                        <h6 class="text-uppercase mb-0">Add New Course</h6>
                    </div>
                    <div class="line"></div>
                    <div class="card-body">
                    <form action="{{ Route('admin.course.store') }}" method="POST"  enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Avatar</label>
                                        <input type="file" placeholder="Course Avatar" name="avatar" class="form-control file-type form-control-lg" required>

                                        @if ($errors->has('avatar'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('avatar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Category</label>
                                        <select name="category" class="form-control form-control-lg" required>
                                            <option disabled>Select Category</option>
                                            <option value="1">Designing</option>
                                            <option value="2">Development</option>
                                            <option value="3" selected>Others</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Name</label>
                                        <input type="text" placeholder="Course Name" name="name" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Short Note</label>
                                        <input type="text" placeholder="Short Note" name="short_note" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label text-uppercase">Course Price</label>
                                        <input type="text" placeholder="Course Price" name="price" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group input_fields_wrap">
                                        <label class="form-control-label text-uppercase" style="display: flow-root;">
                                            <div class="float-left">Course Features</div>
                                            <a href="#" class="add_field_button float-right">Add More Feature</a>
                                        </label>
                                        <input type="text" placeholder="Feature" name="feature[]" class="form-control form-control-lg" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg float-right mt-2">Add New Course</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        {{-- <div class="input_fields_wrap">
                            <button class="add_field_button">Add More Fields</button>
                            <div><input type="text" name="feature[]"></div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Course Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>
$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID

	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<div><input type="text" placeholder="Feature" name="feature[]" class="form-control form-control-lg mt-2" required><a href="#" class="remove_field">Remove</a></div>'); //add input box
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
</script>
@endsection
