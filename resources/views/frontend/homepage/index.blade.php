@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'HOME')

{{--  External CSS Links --}}
@section('css_links')
<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/nice-select.css') }}">
@endsection

{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/homepage/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/homepage/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ===========< Carousel Part Start >=========== --}}
<section class="carousel-part">


    @foreach ($connections as $connection)
        <ul class="home-social">
            <li class="home-social-links"><a href="{{ $connection->instagram }}" target="_blank">Instagram</a></li>
            <li class="home-social-links"><a href="{{ $connection->twitter }}" target="_blank">Twitter</a></li>
            <li class="home-social-links"><a href="{{ $connection->linkedin }}" target="_blank">Linkedin</a></li>
            <li class="home-social-links"><a href="{{ $connection->facebook }}" target="_blank">Facebook</a></li>
        </ul>
    @endforeach


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-image-div p-t-150 p-b-150 p-l-50" style="background-image: url(' {{asset('frontend/images/carousel.jpg') }}');">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Welcome To</h3>
                            <h1>Sylhet Development Academy</h1>
                            <p>It is an educational consultancy, preparation and language training center.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="learn-contact-btn">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{ Route('about') }}" class="btn btn-secondary btn-lg btn-custom btn-learn">Learn More</a>
                                    <a href="{{ Route('contact') }}" class="btn btn-secondary btn-lg btn-custom btn-contact">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< Carousel Part End >============ --}}


{{-- ===========< About Part Start >=========== --}}
<section class="about-part m-b-100" style="background-image: url(' {{asset('frontend/images/about_image.png') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="about-details p-t-100">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="heading">
                                <h6 class="menu-name">About Us</h6>
                                <h2 class="menu-moto">Learn A New Skill With Us</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="details p-t-10">
                                <p class="text">Welcome to Sylhet Development Academy (SDA), it is an educational consultancy, preparation and language training center, with an aim to help the students to achieve their academic goals. SDA is one of the fastest emerging company specializing in education services and test preparation center for Basic computer course.</p>
                                <p class="text p-t-10">Basic Graphic Design, Advance Graphics Design,UI/UX Design, Front-end web development, Back-end web development,  WordPress Development, Digital Marketing, IELTS, GED,  Basic English and English Life Skills.</p>
                            </div>

                            <div class="details-btn p-t-30">
                                <a href="#" class="btn btn-outline-success btn-outline-custom btn-details btn-lg">
                                    Learn More
                                    <span class="arrow">
                                        <img class="green" src="{{ asset('frontend/images/arrow_g.png') }}" alt="">
                                        <img class="white d-none" src="{{ asset('frontend/images/arrow_w.png') }}" alt="">
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< About Part End >============ --}}


{{-- ===========< Address Part Start >=========== --}}
<section class="address-part p-t-70 p-b-25">
    <div class="container">

        @foreach ($connections as $connection)
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-map-marker-alt"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="location">{{ $connection->location }}</li>
                                <li class="city">{{ $connection->city }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-phone-square"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="phone1">{{ $connection->mobile }}</li>
                                <li class="phone2">{{ $connection->phone }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-envelope"></i></td>
                        <td class="td-details">
                            <ul class=" p-l-10">
                                <li class="email1">{{ $connection->contact_mail }}</li>
                                <li class="email2">{{ $connection->info_mail }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @endforeach

    </div>
</section>
{{-- ============< Address Part End >============ --}}


{{-- ===========< Courses Part Start >=========== --}}
<section class="course-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-b-20">
                <div class="row">
                    <div class="col-8">
                        <div class="heading">
                            <h6 class="menu-name">Our Courses</h6></h6>
                            <h2 class="menu-moto">Favorite Course</h2>
                        </div>
                    </div>

                    <div class="col-4 text-right m-t-15">
                        <a href="{{ Route('course') }}" class="btn btn-success btn-lg btn-custom btn-view-all">View All</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="course-carousel">
                    <div class="owl-carousel owl-theme">

                        @foreach ($courses as $course)
                        {{-- Course  --}}
                        <div class="item m-15">
                            <div class="card">
                                <a href="{{ Route('course.show', ['id'=> $course->id]) }}">
                                    <div class="card-body" style="background-image: url(' {{ asset('storage/'.$course->avatar) }} ')"></div>
                                    <div class="card-footer">
                                        <h4>{{ $course->name }}</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< Courses Part End >============ --}}


{{-- ===========< Pricing Part Start >=========== --}}
<section class="pricing-part p-t-50 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="heading">
                    <h6 class="menu-name">Pricing</h6></h6>
                    <h2 class="menu-moto">Choose A Plan</h2>
                </div>
            </div>
        </div>

        <div class="row p-t-30">

            <div class="col-md-6">
                <div class="card">
                    <div class="card-body p-0">

                        {{-- Design Courses --}}
                        <div class="course-heading">
                            <h2 class="fc_w">Design Courses</h2>
                        </div>
                        <table class="table">

                            @foreach ($designs as $design)
                            <tr>
                                <td class="td-no text-right fc_w">&#8250;</td>
                                <td class="td-name">
                                    <ul>
                                        <li class="course-name fc_w">{{ $design->name }}</li>
                                        <li class="course-note fc_w">( {{$design->short_note}} )</li>
                                    </ul>
                                </td>
                                <td class="td-price fc_w">{{$design->price}} <sup>TK</sup></td>
                            </tr>
                            @endforeach

                        </table>

                        {{-- Development Courses --}}
                        <div class="course-heading">
                            <h2 class="fc_w">Development Courses</h2>
                        </div>
                        <table class="table">

                            @foreach ($developments as $development)
                            <tr>
                                <td class="td-no text-right fc_w">&#8250;</td>
                                <td class="td-name">
                                    <ul>
                                        <li class="course-name fc_w">{{ $development->name }}</li>
                                        <li class="course-note fc_w">( {{$development->short_note}} )</li>
                                    </ul>
                                </td>
                                <td class="td-price fc_w">{{$development->price}} <sup>TK</sup></td>
                            </tr>
                            @endforeach

                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-body p-0">

                        {{-- Other Courses --}}
                        <div class="course-heading">
                            <h2 class="fc_w">Other Courses</h2>
                        </div>
                        <table class="table">

                            @foreach ($others as $other)
                            <tr>
                                <td class="td-no text-right fc_w">&#8250;</td>
                                <td class="td-name">
                                    <ul>
                                        <li class="course-name fc_w">{{ $other->name }}</li>
                                        <li class="course-note fc_w">( {{$other->short_note}} )</li>
                                    </ul>
                                </td>
                                <td class="td-price fc_w">{{$other->price}} <sup>TK</sup></td>
                            </tr>
                            @endforeach

                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
{{-- ============< Pricing Part End >============ --}}


{{-- ===========< Contact Part Start >=========== --}}
<section class="contact-part p-b-100" style="background-image: url(' {{asset('frontend/images/contact.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-8">
                <div class="row">
                    <div class="offset-md-2 col-md-10">
                        <div class="heading p-b-30">
                            <h6 class="menu-name">Contact Us</h6></h6>
                            <h2 class="menu-moto">Choose A Course</h2>
                        </div>
                    </div>
                </div>
                <form action="{{ Route('homepage.contact') }}" method="POST">
                @csrf
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Your Email*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control nice-select" id="course" name="course" required>
                                        <option data-display="Select Course" disabled selected>Select Course</option>
                                        <option disabled>Designing Courses</option>
                                        @foreach ($designs as $design)
                                            <option value="{{ $design->name }}">{{ $design->name }}</option>
                                        @endforeach

                                        <option disabled>Development Courses</option>
                                        @foreach ($developments as $development)
                                            <option value="{{ $development->name }}">{{ $development->name }}</option>
                                        @endforeach

                                        <option disabled>Other Courses</option>
                                        @foreach ($others as $other)
                                            <option value="{{ $other->name }}">{{ $other->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="message" name="message" rows="3" placeholder="Message" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-secondary btn-lg btn-custom btn-contact" type="submit">Send Request</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
{{-- ============< Contact Part End >============ --}}
@endsection




{{--  External Javascript Links --}}
@section('script_links')
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.nice-select.min.js') }}"></script>
@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/homepage/script.js') }}"></script>
<script src="{{ asset('frontend/js/homepage/responsive.js') }}"></script>
@endsection
