@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'CONTACT')

{{--  External CSS Links --}}
@section('css_links')
<link rel="stylesheet" href="{{ asset('frontend/css/nice-select.css') }}">
@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/contact/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/contact/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ===========< Contact Part Start >=========== --}}
<section class="contact-part p-b-100" style="background-image: url(' {{asset('frontend/images/contact.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-8">
                <div class="row">
                    <div class="offset-md-2 col-md-10">
                        <div class="heading p-t-30 p-b-10">
                            <h6 class="menu-name">Contact Us</h6></h6>
                            <h2 class="menu-moto">Choose A Course</h2>
                        </div>
                    </div>
                </div>
                <form action="{{ Route('homepage.contact') }}" method="POST">
                @csrf
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Your Email*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number*" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control nice-select" id="course" name="course" required>
                                        <option data-display="Select Course" disabled selected>Select Course</option>
                                        <option disabled>Designing Courses</option>
                                        @foreach ($designs as $design)
                                            <option value="{{ $design->name }}">{{ $design->name }}</option>
                                        @endforeach

                                        <option disabled>Development Courses</option>
                                        @foreach ($developments as $development)
                                            <option value="{{ $development->name }}">{{ $development->name }}</option>
                                        @endforeach

                                        <option disabled>Other Courses</option>
                                        @foreach ($others as $other)
                                            <option value="{{ $other->name }}">{{ $other->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="message" name="message" rows="3" placeholder="Message" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-secondary btn-lg btn-custom btn-contact" type="submit">Send Request</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
{{-- ============< Contact Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')
<script src="{{ asset('frontend/js/jquery.nice-select.min.js') }}"></script>
@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/contact/script.js') }}"></script>
<script src="{{ asset('frontend/js/contact/responsive.js') }}"></script>
@endsection
