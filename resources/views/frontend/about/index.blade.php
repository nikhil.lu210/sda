@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'ABOUT')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/about/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/about/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ===========< Carousel Part Start >=========== --}}
<section class="carousel-part">


    @foreach ($connections as $connection)
        <ul class="home-social">
            <li class="home-social-links"><a href="{{ $connection->instagram }}" target="_blank">Instagram</a></li>
            <li class="home-social-links"><a href="{{ $connection->twitter }}" target="_blank">Twitter</a></li>
            <li class="home-social-links"><a href="{{ $connection->linkedin }}" target="_blank">Linkedin</a></li>
            <li class="home-social-links"><a href="{{ $connection->facebook }}" target="_blank">Facebook</a></li>
        </ul>
    @endforeach


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-image-div p-t-170 p-b-170 p-l-50" style="background-image: url(' {{asset('frontend/images/about_details.jpg') }}');">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>About Us</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< Carousel Part End >============ --}}


{{-- ===========< About Part Start >=========== --}}
<section class="about-part" style="background-image: url(' {{asset('frontend/images/about_image.png') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="about-details">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="heading">
                                <h2 class="menu-moto">Learn A New Skill With Us</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="details p-t-10">
                                <p class="text">Welcome to Sylhet Development Academy (SDA), it is an educational consultancy, preparation and language training center, with an aim to help the students to achieve their academic goals. SDA is one of the fastest emerging company specializing in education services and test preparation center for Basic computer course.</p>
                                <p class="text p-t-10">Basic Graphic Design, Advance Graphics Design,UI/UX Design, Front-end web development, Back-end web development,  WordPress Development, Digital Marketing, IELTS, GED,  Basic English and English Life Skills.</p>
                                <p class="text p-t-10">SDA offers a broad portfolio of excellently managed services tailored to the needs of each and every individual seeking the best education. We have developed a complete business model, only one of its kinds, to provide one-stop package of services to make an individual's transition completely hassle-free.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< About Part End >============ --}}


{{-- ===========< Address Part Start >=========== --}}
<section class="address-part p-t-70 p-b-25">
    <div class="container">

        @foreach ($connections as $connection)
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-map-marker-alt"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="location">{{ $connection->location }}</li>
                                <li class="city">{{ $connection->city }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-phone-square"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="phone1">{{ $connection->mobile }}</li>
                                <li class="phone2">{{ $connection->phone }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-envelope"></i></td>
                        <td class="td-details">
                            <ul class=" p-l-10">
                                <li class="email1">{{ $connection->contact_mail }}</li>
                                <li class="email2">{{ $connection->info_mail }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @endforeach

    </div>
</section>
{{-- ============< Address Part End >============ --}}


{{-- ===========< About Part Start >=========== --}}
<section class="about-part-2" style="background-image: url(' {{asset('frontend/images/about_image_2.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-md-10 p-t-50">
                            <div class="heading">
                                <h2 class="menu-moto">We are independent and trusted</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="details p-t-10 p-b-50">
                                <p class="text">Our core activity lies in assisting students to make the right choice in pursuing their education in overseas educational institutions as well as providing academic and professional trainings for them to have a bright future. </p>
                                <p class="text p-t-10">We are independent and trusted: established in 2019, the founder of this institute is from United Kingdom and is supported by UK based educational institutes. SDA is motivation-driven. Our independence is the cornerstone of our credibility. It allows us to develop fresh ideas and take the risks we need to succeed. <br> At a time when the world is faced with complex dilemmas, innovation will be essential to finding and testing bold ideas and solutions. We will be increasingly innovative in the ways in which we generate ideas, bring people together, communicate our work and increase our influence.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< About Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/about/script.js') }}"></script>
<script src="{{ asset('frontend/js/about/responsive.js') }}"></script>
@endsection
