@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'COURSE || DETAILS')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/course/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/course/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ===========< Carousel Part Start >=========== --}}
<section class="carousel-part">


    @foreach ($connections as $connection)
        <ul class="home-social">
            <li class="home-social-links"><a href="{{ $connection->instagram }}" target="_blank">Instagram</a></li>
            <li class="home-social-links"><a href="{{ $connection->twitter }}" target="_blank">Twitter</a></li>
            <li class="home-social-links"><a href="{{ $connection->linkedin }}" target="_blank">Linkedin</a></li>
            <li class="home-social-links"><a href="{{ $connection->facebook }}" target="_blank">Facebook</a></li>
        </ul>
    @endforeach


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-image-div p-t-170 p-b-170 p-l-50" style="background-image: url(' {{asset('frontend/images/about_details.jpg') }}');">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>{{ $course->name }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< Carousel Part End >============ --}}


{{-- ===========< course Part Start >=========== --}}
<section class="course-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2 class="menu-moto">{{ $course->name }}</h2>
                </div>
                <div class="course-moto">
                    <h4 class="menu-moto">{{ $course->short_note }}</h4>
                </div>
            </div>
        </div>

        <div class="row p-t-30 p-b-50">
            <div class="col-md-8">
                <p class="text">Our Each Class is Designed with basis of Classes. This Course Offers you following classes: </p>
            </div>

            <div class="col-md-8">
                <div class="classes">
                    @foreach ($course->feature as $key => $feature)
                        <p><span class="text-bold">Class {{$key+1}}:</span> <span class="text">{{ $feature }}.</span></p>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< course Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/course/script.js') }}"></script>
<script src="{{ asset('frontend/js/course/responsive.js') }}"></script>
@endsection
