@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'COURSES')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/course/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/course/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ===========< Carousel Part Start >=========== --}}
<section class="carousel-part">


    @foreach ($connections as $connection)
        <ul class="home-social">
            <li class="home-social-links"><a href="{{ $connection->instagram }}" target="_blank">Instagram</a></li>
            <li class="home-social-links"><a href="{{ $connection->twitter }}" target="_blank">Twitter</a></li>
            <li class="home-social-links"><a href="{{ $connection->linkedin }}" target="_blank">Linkedin</a></li>
            <li class="home-social-links"><a href="{{ $connection->facebook }}" target="_blank">Facebook</a></li>
        </ul>
    @endforeach


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel-image-div p-t-170 p-b-170 p-l-50" style="background-image: url(' {{asset('frontend/images/about_details.jpg') }}');">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Our Courses</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ============< Carousel Part End >============ --}}


{{-- ===========< course Part Start >=========== --}}
<section class="course-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2 class="menu-moto">Design & Development</h2>
                </div>
            </div>
        </div>

        <div class="row p-t-30">

            {{-- Design Courses --}}
            @foreach ($designs as $design)
            <div class="col-md-4 m-b-20">
                <div class="card courses">
                    <div class="card-body" style="background-image: url(' {{ asset('storage/'.$design->avatar) }} ')">
                        <div class="overlay">
                            <a href="{{ Route('course.show', ['id'=> $design->id]) }}" class="btn btn-outline-success btn-outline-custom btn-details">Details</a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h4>Basic Graphics Design</h4>
                    </div>
                </div>
            </div>
            @endforeach

            {{-- Development Courses --}}
            @foreach ($developments as $development)
            <div class="col-md-4 m-b-20">
                <div class="card courses">
                    <div class="card-body" style="background-image: url(' {{ asset('storage/'.$development->avatar) }} ')">
                        <div class="overlay">
                            <a href="{{ Route('course.show', ['id'=> $development->id]) }}" class="btn btn-outline-success btn-outline-custom btn-details">Details</a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h4>Basic Graphics Design</h4>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
{{-- ============< course Part End >============ --}}


{{-- ===========< Address Part Start >=========== --}}
<section class="address-part p-t-70 p-b-25">
    <div class="container">

        @foreach ($connections as $connection)
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-map-marker-alt"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="location">{{ $connection->location }}</li>
                                <li class="city">{{ $connection->city }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-phone-square"></i></td>
                        <td class="td-details">
                            <ul>
                                <li class="phone1">{{ $connection->mobile }}</li>
                                <li class="phone2">{{ $connection->phone }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td class="td-icon"><i class="fas fa-envelope"></i></td>
                        <td class="td-details">
                            <ul class=" p-l-10">
                                <li class="email1">{{ $connection->contact_mail }}</li>
                                <li class="email2">{{ $connection->info_mail }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @endforeach

    </div>
</section>
{{-- ============< Address Part End >============ --}}


{{-- ===========< course Part Start >=========== --}}
<section class="course-part">
    <div class="container">
        <div class="row p-t-50">
            <div class="col-md-12">
                <div class="heading">
                    <h2 class="menu-moto">Other Courses</h2>
                </div>
            </div>
        </div>

        <div class="row p-t-30">

            {{-- Design Courses --}}
            @foreach ($others as $other)
            <div class="col-md-4 m-b-20">
                <div class="card courses">
                    <div class="card-body" style="background-image: url(' {{ asset('storage/'.$other->avatar) }} ')">
                        <div class="overlay">
                            <a href="{{ Route('course.show', ['id'=> $other->id]) }}" class="btn btn-outline-success btn-outline-custom btn-details">Details</a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h4>{{ $other->name }}</h4>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
{{-- ============< course Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/course/script.js') }}"></script>
<script src="{{ asset('frontend/js/course/responsive.js') }}"></script>
@endsection
