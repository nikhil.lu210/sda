<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow"> {{-- Title --}}
    <title>SDA || ADMIN || LOGIN</title>
    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('frontend/images/title.png') }}">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}"> {{--
    <link rel="stylesheet" href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css') }}"> --}} {{-- Font-Awesome CSS --}}
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome/all.min.css') }}">

    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">

    <!-- orion icons-->
    <link rel="stylesheet" href="{{ asset('backend/css/orionicons.css') }}">

    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.default.css') }}" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">

    <style>
        a.external{
            font-weight: 600;
            color: #666666;
            text-decoration: none;
        }
        a.external:hover{
            color: #01b402;
        }
    </style>
</head>

<body>
    <div class="page-holder d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center py-5">
                <div class="col-5 col-lg-7 mx-auto mb-5 mb-lg-0">
                    <div class="pr-lg-5"><img src="{{ asset('frontend/images/logo.png') }}" alt="" class="img-fluid"></div>
                </div>
                <div class="col-lg-5 px-lg-4">
                    <h1 class="text-base text-primary text-uppercase mb-4">Sylhet Development Academy</h1>
                    <h2 class="mb-4">Admin Login</h2>
                    <p class="text-muted">Login as a Admin to Sylhet Development Academy.</p>
                    <form id="loginForm" class="mt-4" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group mb-4">
                            <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" autofocus>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-4">
                            <div class="custom-control custom-checkbox">
                                <input id="customCheck1" type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="customCheck1" class="custom-control-label">Remember Me</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg px-5 float-right">LOGIN</button>
                    </form>
                </div>
            </div>
            <p class="mt-5 mb-0 text-gray-400 text-center">DEVELOPED BY <a href="https://www.veechitechnologies.com" class="external">VEECHI TECHNOLOGIES</a></p>
        </div>
    </div>
    <!-- JavaScript files-->
    <script src="{{ asset('backend/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/vendor/popper.js/umd/popper.min.js') }}"></script>
    <script src="{{ asset('backend/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/vendor/jquery.cookie/jquery.cookie.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="{{ asset('backend/js/front.js') }}"></script>
</body>

</html>
