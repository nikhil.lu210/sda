<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'SDA NIKHIL',
            'email' => 'sda@admin.com',
            'password' => bcrypt('SDA@Admin2019')
        ]);
    }
}
