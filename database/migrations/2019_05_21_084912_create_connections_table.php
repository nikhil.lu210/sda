<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location', 200);
            $table->string('city', 50);
            $table->string('mobile', 50);
            $table->string('phone', 50);
            $table->string('contact_mail', 50);
            $table->string('info_mail', 50);
            $table->string('facebook', 100);
            $table->string('linkedin', 100);
            $table->string('twitter', 100)->default('#');
            $table->string('instagram', 100)->default('#');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
