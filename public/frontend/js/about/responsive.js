$(document).ready(function($) {
    function mediaSize() {
        // var wSize = $(window).width();
        /* Set the matchMedia */

        /*===============================================
        ==========< Device Maximum 575.98px >============
        =================================================*/
        if (window.matchMedia('(max-width: 575.98px)').matches) {
            // Carousel Part
            $('.home-social').addClass('d-none');
            $('.carousel-image-div p').addClass('d-none');
            $('.carousel-image-div').removeClass('p-t-170 p-b-170 p-l-50').addClass('p-t-100 p-b-100');
            $('.carousel-image-div h1').addClass('p-t-50');


            // About Part
            $('.about-part').removeClass('m-b-100').addClass('m-b-20');
            $('.about-details').removeClass('p-t-100').addClass('p-t-0');


            // About Part
            $('.about-part-2').removeClass('m-b-100').addClass('m-b-20');
        }


        /*=============================================================
        ==========< Device Size Between 576px to 768px >============
        =============================================================*/
        else if (window.matchMedia('(min-width: 576px)' && '(max-width: 768px)').matches) {
            // Carousel Part
            $('.home-social').addClass('d-none');
            $('.carousel-image-div').removeClass('p-t-150 p-b-150 p-l-50').addClass('p-t-50 p-b-50 p-l-50');
            $('.carousel-image-div .col-md-8').removeClass('col-md-8').addClass('col-md-12');
            $('.about-details').parent().removeClass('col-md-7').addClass('col-md-10');


            // Address Part
            $('.address-part').removeClass('p-t-70 p-b-25').addClass('p-t-30 p-b-5');
            $('.address-part .col-md-4').removeClass('col-md-4').addClass('col-md-6');


            // About 2 Part
            $('.about-part-2 .about-details').parent().removeClass('col-md-6 offset-md-6').addClass('col-md-8 offset-md-2');
            $('.about-part-2 .heading').parent().removeClass('col-md-10').addClass('col-md-7 offset-md-5');
        }


        /*=============================================================
        ==========< Device Size Between 768px to 991.98px >============
        ===============================================================*/
        else if (window.matchMedia('(min-width: 768.1px)' && '(max-width: 991.98px)').matches) {

        }


        /*==============================================================
        ==========< Device Size Between 992px to 1199.98px >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 992px)' && '(max-width: 1199.98px)').matches) {

        }


        /*================================================================
        ==========< Device Size Between 1200px to infinty :p >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 1200px)').matches) {

        }


        /*==========================================================
        ==========< Device Size if those are not match >============
        ==========================================================*/
        else {

        }
    };

    /* Call the function */
    mediaSize();
    /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);

});
