$(document).ready(function($) {
    // Code Here
    $('.btn-details').hover(
        function() {
            $('.arrow .green').addClass('d-none');
            $('.arrow .white').removeClass('d-none');
        }, function() {
            $('.arrow .white').addClass('d-none');
            $('.arrow .green').removeClass('d-none');
        }
    );



    // Carousel
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:true
            },
            1000:{
                items:3,
                nav:true,
                loop:false
            }
        }
    });

    $('.owl-prev span').html('<i class="fas fa-arrow-left"></i>');
    $('.owl-next span').html('<i class="fas fa-arrow-right"></i>');


    $('.pricing-part .card').hover(
        function() {
            $(this).addClass('bg-green');
        }, function() {
            $(this).removeClass('bg-green');
        }
    );



    // Contact Part
    $('.nice-select').niceSelect();
});
