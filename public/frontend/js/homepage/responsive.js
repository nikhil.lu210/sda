$(document).ready(function($) {
    function mediaSize() {
        // var wSize = $(window).width();
        /* Set the matchMedia */

        /*===============================================
        ==========< Device Maximum 575.98px >============
        =================================================*/
        if (window.matchMedia('(max-width: 575.98px)').matches) {
            // Carousel Part
            $('.home-social').addClass('d-none');
            $('.carousel-image-div p').addClass('d-none');
            $('.carousel-image-div').removeClass('p-t-150 p-b-150 p-l-50');


            // About Part
            $('.about-part').removeClass('m-b-100').addClass('m-b-20');
            $('.about-details').removeClass('p-t-100').addClass('p-t-30');

            // Course Part
            $('.course-part').removeClass('p-t-100 p-b-100').addClass('p-t-50 p-b-50');

            // Pricing Part
            $('.pricing-part').removeClass('p-b-100').addClass('p-b-50');

            // contact Part
            $('.contact-part').removeClass('p-b-100').addClass('p-t-30 p-b-50');
            $('.contact-form .btn-custom.btn-contact').addClass('btn-block');

            // footer Part
            $('.footer-part').removeClass('p-t-100 p-b-50').addClass('p-t-30 p-b-20');
            $('.quick-links').parent().removeClass('col-md-3').addClass('col-6');
            $('.social-links').parent().removeClass('col-md-3').addClass('col-6');

            // bottom Part
            $('.bottom-part').removeClass('p-t-30 p-b-30').addClass('p-t-10 p-b-10');

        }


        /*=============================================================
        ==========< Device Size Between 576px to 767.98px >============
        =============================================================*/
        else if (window.matchMedia('(min-width: 576px)' && '(max-width: 768px)').matches) {
            // Carousel Part
            $('.home-social').addClass('d-none');
            $('.carousel-image-div').removeClass('p-t-150 p-b-150 p-l-50').addClass('p-t-50 p-b-50 p-l-50');
            $('.carousel-image-div .col-md-8').removeClass('col-md-8').addClass('col-md-12');
            $('.about-details').parent().removeClass('col-md-7').addClass('col-md-10');


            // Address Part
            $('.address-part').removeClass('p-t-70 p-b-25').addClass('p-t-30 p-b-5');
            $('.address-part .col-md-4').removeClass('col-md-4').addClass('col-md-6');


            // contact Part
            $('.contact-part .heading').parent().removeClass('offset-md-2 col-md-10').addClass('col-md-12');
        }


        /*=============================================================
        ==========< Device Size Between 768px to 991.98px >============
        ===============================================================*/
        else if (window.matchMedia('(min-width: 768.1px)' && '(max-width: 991.98px)').matches) {

        }


        /*==============================================================
        ==========< Device Size Between 992px to 1199.98px >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 992px)' && '(max-width: 1199.98px)').matches) {

        }


        /*================================================================
        ==========< Device Size Between 1200px to infinty :p >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 1200px)').matches) {

        }


        /*==========================================================
        ==========< Device Size if those are not match >============
        ==========================================================*/
        else {

        }
    };

    /* Call the function */
    mediaSize();
    /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);

});
