<?php

namespace App\Http\Controllers\Backend\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Course;
use Illuminate\Support\Facades\File;
use Storage;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('backend.course.index')->withCourses($courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, array(
            'avatar' => 'required',
            'category' => 'required',
            'name' => 'required',
            'short_note' => 'required',
            'price' => 'required',
            'feature' => 'required',
        ));

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $mime = $avatar->getMimeType();

            if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
                $this->validate($request,array(
                    'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
                ));
            }
        }

        $course = new Course();

        // if avatar is present
        if($request->hasFile('avatar')){

            $course->avatar = $request->file('avatar')->store('avatars', 'public');

            // $request->avatar->move(public_path('uploaded_image'), $path);

            // $course->avatar = $path;
        }

        $course->category = $request->category;
        $course->name = $request->name;
        $course->short_note = $request->short_note;
        $course->price = $request->price;
        $course->feature = $request->feature;

        // dd($course);
        $course->save();

        return redirect()->Route('admin.course');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);
        return view('backend.course.show')->withCourse($course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'avatar' => 'required',
            'category' => 'required',
            'name' => 'required',
            'short_note' => 'required',
            'feature' => 'required',
            'price' => 'required',
        ));


        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $mime = $avatar->getMimeType();

            if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
                $this->validate($request,array(
                    'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
                ));
            }
        }

        $course = Course::find($id);
        if($request->hasFile('avatar')){

            unlink(storage_path('app/public/'.$course->avatar));

            $course->avatar = $request->file('avatar')->store('avatars', 'public');
        }

        $course->category = $request->category;
        $course->name = $request->name;
        $course->short_note = $request->short_note;
        $course->feature = $request->feature;
        $course->price = $request->price;

        // dd($course);
        $course->save();

        return redirect()->Route('admin.course');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        unlink(storage_path('app/public/'.$course->avatar));

        $course->delete();

        return redirect()->back();
    }
}
