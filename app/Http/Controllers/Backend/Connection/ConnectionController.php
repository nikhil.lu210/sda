<?php

namespace App\Http\Controllers\Backend\Connection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Connection;

class ConnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $connections = Connection::all();
        return view('backend.connection.index')->withConnections($connections);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.connection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'location' => 'required',
            'city' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'contact_mail' => 'required',
            'info_mail' => 'required',
            'facebook' => 'required',
            'linkedin' => 'required',
        ));

        $connection = new Connection;

        $connection->location = $request->location;
        $connection->city = $request->city;
        $connection->mobile = $request->mobile;
        $connection->phone = $request->phone;
        $connection->contact_mail = $request->contact_mail;
        $connection->info_mail = $request->info_mail;
        $connection->facebook = $request->facebook;
        $connection->linkedin = $request->linkedin;
        $connection->twitter = $request->twitter;
        $connection->instagram = $request->instagram;

        // dd($connection);
        $connection->save();

        return redirect()->Route('admin.connection');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connection = Connection::find($id);
        return view('backend.connection.show')->withConnection($connection);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'location' => 'required',
            'city' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'contact_mail' => 'required',
            'info_mail' => 'required',
            'facebook' => 'required',
            'linkedin' => 'required',
        ));

        $connection = Connection::find($id);

        $connection->location = $request->location;
        $connection->city = $request->city;
        $connection->mobile = $request->mobile;
        $connection->phone = $request->phone;
        $connection->contact_mail = $request->contact_mail;
        $connection->info_mail = $request->info_mail;
        $connection->facebook = $request->facebook;
        $connection->linkedin = $request->linkedin;
        $connection->twitter = $request->twitter;
        $connection->instagram = $request->instagram;

        // dd($connection);
        $connection->save();

        return redirect()->Route('admin.connection');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
