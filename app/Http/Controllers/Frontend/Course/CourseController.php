<?php

namespace App\Http\Controllers\Frontend\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Backend\Connection;
use App\Model\Backend\Course;
use App\Model\Backend\Contact;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $connections = Connection::all();

        $courses = Course::all();

        $design_courses = Course::select('id', 'avatar', 'name', 'short_note', 'price')
                                ->where('category', 1)
                                ->get();

        $development_courses = Course::select('id', 'avatar', 'name', 'short_note', 'price')
                                ->where('category', 2)
                                ->get();

        $other_courses = Course::select('id', 'avatar', 'name', 'short_note', 'price')
                                ->where('category', 3)
                                ->get();

        return view('frontend.course.index')->withConnections($connections)
                                            ->withCourses($courses)
                                            ->withDesigns($design_courses)
                                            ->withDevelopments($development_courses)
                                            ->withOthers($other_courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connections = Connection::all();

        $course = Course::find($id);

        return view('frontend.course.show')->withConnections($connections)
                                            ->withCourse($course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
