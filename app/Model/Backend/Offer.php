<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'course',
        'name',
        'discount',
        'note',
    ];
}
