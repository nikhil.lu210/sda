<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $fillable =[
        'location',
        'city',
        'mobile',
        'phone',
        'contact_mail',
        'info_mail',
        'facebook',
        'linkedin',
        'twitter',
        'instagram',
    ];
}
