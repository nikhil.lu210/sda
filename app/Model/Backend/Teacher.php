<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'avatar',
        'name',
        'email',
        'number',
        'joining_date',
        'basic_salary',
        'course',
        'note',
    ];
}
