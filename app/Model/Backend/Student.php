<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'avatar',
        'batch',
        'student_id',
        'name',
        'email',
        'number',
        'admission_date',
        'course',
        'note',
    ];
}
