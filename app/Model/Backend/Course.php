<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'category',
        'name',
        'avatar',
        'short_note',
        'price',
        'feature',
    ];

    protected $casts = [
        'id' => 'int',
        'feature' => 'array',
    ];
}
