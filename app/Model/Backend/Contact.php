<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'course',
        'message',
    ];
}
