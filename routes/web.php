<?php

// Auth::routes();
include_once 'auth/auth_route/auth_route.php';

// Frontend Routes (Guest)
Route::group(
    [
        'middleware' => ['guest' || 'auth']
    ],
    function () {

        // Homepage
        include_once 'frontend/homepage/homepage.php';

        // About
        include_once 'frontend/about/about.php';

        // Cource
        include_once 'frontend/course/course.php';

        // Offer
        include_once 'frontend/offer/offer.php';

        // Contact
        include_once 'frontend/contact/contact.php';

    }
);


// Backend Routes (Admin)
Route::group(
    [
        'middleware' => ['auth']
    ],
    function () {

        // Dashboard
        include_once 'backend/dashboard/dashboard.php';

        // contact
        include_once 'backend/contact/contact.php';

        // course
        include_once 'backend/course/course.php';

        // offer
        include_once 'backend/offer/offer.php';

        // teacher
        include_once 'backend/teacher/teacher.php';

        // student
        include_once 'backend/student/student.php';

        // connection
        include_once 'backend/connection/connection.php';
    }
);
