<?php

// Admin Dashboard Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Dashboard', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('dashboard', 'DashboardController@index')->name('admin');
    }
);
