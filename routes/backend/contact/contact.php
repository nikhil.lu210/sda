<?php

// Admin Contact Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Contact', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('contact', 'ContactController@index')->name('contact');
        Route::get('contact/create', 'ContactController@create')->name('contact.create');
        Route::get('contact/show/{id}', 'ContactController@show')->name('contact.show');
        Route::get('contact/destroy/{id}', 'ContactController@destroy')->name('contact.destroy');
    }
);
