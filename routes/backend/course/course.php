<?php

// Admin Course Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Course', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('records/course', 'CourseController@index')->name('course');
        Route::get('settings/course', 'CourseController@create')->name('course.create');
        Route::post('settings/course/store', 'CourseController@store')->name('course.store');
        Route::get('records/course/show/{id}', 'CourseController@show')->name('course.show');
        Route::post('records/course/update/{id}', 'CourseController@update')->name('course.update');
        Route::get('records/course/destroy/{id}', 'CourseController@destroy')->name('course.destroy');
    }
);
