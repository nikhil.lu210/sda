<?php

// Admin Offer Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Offer', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('records/offer', 'OfferController@index')->name('offer');
        Route::get('settings/offer', 'OfferController@create')->name('offer.create');
    }
);
