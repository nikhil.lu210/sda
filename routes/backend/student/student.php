<?php

// Admin Student Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Student', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('records/student', 'StudentController@index')->name('student');
        Route::get('settings/student', 'StudentController@create')->name('student.create');
    }
);
