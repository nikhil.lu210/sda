<?php

// Admin Connection Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Connection', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('records/connection', 'ConnectionController@index')->name('connection');
        Route::get('settings/connection', 'ConnectionController@create')->name('connection.create');
        Route::post('settings/connection/store', 'ConnectionController@store')->name('connection.store');
        Route::get('records/connection/show/{id}', 'ConnectionController@show')->name('connection.show');
        Route::post('records/connection/update/{id}', 'ConnectionController@update')->name('connection.update');
    }
);
