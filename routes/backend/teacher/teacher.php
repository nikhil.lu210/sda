<?php

// Admin Teacher Routes
Route::group([
    'prefix' => 'admin', //For Url
    'namespace' => 'Backend\Teacher', //For Controller
    'as' => 'admin.' //For Route Name
],
    function(){
        Route::get('records/teacher', 'TeacherController@index')->name('teacher');
        Route::get('settings/teacher', 'TeacherController@create')->name('teacher.create');
    }
);
