<?php

// Offer Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Offer', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('offers', 'OfferController@index')->name('offer');
    }
);
