<?php

// Homepage Routes
Route::group([
    'prefix' => '/', //For Url
    'namespace' => 'Frontend\Homepage', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('', 'HomepageController@index')->name('homepage');
        Route::post('contact/store', 'HomepageController@store')->name('homepage.contact');
    }
);
