<?php

// About Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\About', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('about', 'AboutController@index')->name('about');
    }
);
