<?php

// Course Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Course', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('courses', 'CourseController@index')->name('course');
        Route::get('courses/details/{id}', 'CourseController@show')->name('course.show');
    }
);
